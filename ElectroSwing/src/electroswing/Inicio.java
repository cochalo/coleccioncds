package electroswing;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import clases.Electrodomestico;
import clases.Lavadora;
import clases.Television;
import net.miginfocom.swing.MigLayout;
import javax.swing.JScrollPane;
import java.awt.SystemColor;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Inicio extends JFrame {
	private JPanel contentPane;
	private JPanel pnlTV;
	private InternalTV nuevo;
        private Television newtv;
        private Lavadora newwash;
	private ArrayList<Electrodomestico>lista=new ArrayList<>();
        private ArrayList<Television>TVlista=new ArrayList<Television>();
        private MarcoTV ventanaTV=new MarcoTV();
        
    private void rellenar(){
        Electrodomestico[] stock=new Electrodomestico[10];
        stock[0]=new Lavadora();
        stock[0].setNombre("LG");
        stock[1]=new Lavadora(120, 80);
        stock[1].setNombre("LG");
        stock[2]=new Lavadora(80, 280, 300, "Negro", 'A');
        stock[2].setNombre("LG");
        stock[3]=new Lavadora();
        stock[3].setNombre("LG");
        stock[4]=new Lavadora(80, 400, 80, "azul", 'F');
        stock[4].setNombre("LG");
        stock[5]=new Television();
        stock[5].setNombre("LG");
        stock[6]=new Television(140, 60);
        stock[6].setNombre("LG");
        stock[7]=new Television(500, 40, "rojo", 'A', 100, true);
        stock[7].setNombre("LG");
        stock[8]=new Television();
        stock[8].setNombre("LG");
        stock[9]=new Television(200, 30);
        stock[9].setNombre("LG");
        for (int i = 0; i < stock.length; i++) {
          lista.add(stock[i]);
        }
    }
    private void CargarDatos() {
		int i = 0;
		for (Electrodomestico al : TVlista) {
			if (al.getTipo().equals("TV")) {
				nuevo = new InternalTV(al);
				pnlTV.add(nuevo, "cell 0 " + i + ",grow");
				nuevo.setVisible(true);
			}
			i++;
		}
                pack();
                setExtendedState(JFrame.MAXIMIZED_BOTH);/*pantalla maxima*/
    }
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Inicio frame = new Inicio();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
       
	public Inicio() {
            ventanaTV.listaTV = this.TVlista;
            
            setTitle("ElectroSwing");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setExtendedState(JFrame.MAXIMIZED_BOTH);/*pantalla maxima*/
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnCrear = new JMenu("Crear");
		menuBar.add(mnCrear);
		
		JMenuItem mntmTelevision = new JMenuItem("Television");
		mntmTelevision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
                            ventanaTV.setVisible(true);
			}
		});
		mnCrear.add(mntmTelevision);
		
		JMenuItem mntmLavadora = new JMenuItem("Lavadora");
		mnCrear.add(mntmLavadora);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow][grow]", "[grow]"));
		
		JScrollPane scrTV = new JScrollPane();
		contentPane.add(scrTV, "cell 0 0,grow");
		
		pnlTV = new JPanel();
		pnlTV.setBackground(SystemColor.textHighlight);
		scrTV.setViewportView(pnlTV);
		pnlTV.setLayout(new MigLayout("", "[]", "[]"));
		
		JScrollPane scrWash = new JScrollPane();
		contentPane.add(scrWash, "cell 1 0,grow");
		
		JPanel pnlWash = new JPanel();
		pnlWash.setBackground(SystemColor.activeCaption);
		scrWash.setViewportView(pnlWash);
		pnlWash.setLayout(new MigLayout("", "[]", "[]"));
		//rellenar();
        	addFocusListener(new java.awt.event.FocusAdapter() {
                    public void focusGained(java.awt.event.FocusEvent evt) {
                        formFocusGained(evt);
                    }
                });
	}
        private void formFocusGained(java.awt.event.FocusEvent evt) {                                 
            CargarDatos();
    }    
}
