package electroswing;

import javax.swing.JPanel;
import java.awt.SystemColor;
import net.miginfocom.swing.MigLayout;
import javax.swing.JTextField;

import clases.Electrodomestico;
import clases.Television;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InternalTV extends JPanel {
	private JTextField txtNombre;
	private JTextField txtPrecio;
	private JButton btnNewButton;
	public InternalTV(Electrodomestico x) {
		setBackground(SystemColor.textHighlight);
		setLayout(new MigLayout("", "[grow][grow][]", "[]"));
		
		txtNombre = new JTextField();
		txtNombre.setEditable(false);
		txtNombre.setText(x.getNombre());
		add(txtNombre, "cell 0 0,growx");
		txtNombre.setColumns(10);
		
		txtPrecio = new JTextField();
		txtPrecio.setEditable(false);
		txtPrecio.setText(String.valueOf(x.getPrecio_base()));
		add(txtPrecio, "cell 1 0,growx");
		txtPrecio.setColumns(10);
		
		btnNewButton = new JButton("Ver");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		add(btnNewButton, "cell 2 0");

	}

}
