package clases;
public class Lavadora extends Electrodomestico{
    float carga;

    public Lavadora() {
        carga=5;
    }

    public Lavadora(float precio_base, float peso) {
        super(precio_base, peso);
        carga=5;
    }

    public Lavadora(float carga, float precio_base, float peso, String color, char consumo_energetico) {
        super(precio_base, peso, color, consumo_energetico);
        this.carga = carga;
    }
    
    public float getCarga() {
        return carga;
    }
    @Override
    public String getTipo() {
    	return "wash";
    }
    @Override
    public void precioFinal() {
        super.precioFinal(); 
        if (carga>30) {
            precio_base+=50;
        } 
    }
}
