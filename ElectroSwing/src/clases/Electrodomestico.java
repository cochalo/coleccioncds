package clases;
public class Electrodomestico {
    protected String tipo="Electrodomestico";
    protected String nombre;
    protected float precio_base,peso;
    protected String color;
    protected char consumo_energetico;
    protected String[] colores_disponibles={"blanco","negro","rojo","azul","gris"};
    protected char[] letras_consumo={'A','B','C','D','E','F'};
    
    public Electrodomestico() {
        precio_base=100;
        peso=5;
        color="blanco";
        consumo_energetico='F';
        tipo="Electrodomestico";
    }

    public Electrodomestico(float precio_base, float peso) {
        this.precio_base = precio_base;
        this.peso = peso;
        color="blanco";
        consumo_energetico='F';
        tipo="Electrodomestico";
    }

    public Electrodomestico(float precio_base, float peso, String color, char consumo_energetico) {
        this.precio_base = precio_base;
        this.peso = peso;
        this.color = color;
        this.consumo_energetico = comprobarConsumoEnergetico(consumo_energetico);
        tipo="Electrodomestico";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public float getPrecio_base() {
        return precio_base;
    }

    public void setPrecio_base(float precio_base) {
        this.precio_base = precio_base;
    }
    

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }
    

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public char getConsumo_energetico() {
        return consumo_energetico;
    }

    public void setConsumo_energetico(char consumo_energetico) {
        this.consumo_energetico = consumo_energetico;
    }

    public String[] getColores_disponibles() {
        return colores_disponibles;
    }

    public void setColores_disponibles(String[] colores_disponibles) {
        this.colores_disponibles = colores_disponibles;
    }
    

    public char[] getLetras_consumo() {
        return letras_consumo;
    }

    public void setLetras_consumo(char[] letras_consumo) {
        this.letras_consumo = letras_consumo;
    }
    
    protected char comprobarConsumoEnergetico(char letra){
        boolean flag1=false;
        for (int i = 0; i < letras_consumo.length; i++) {
            if (letra==letras_consumo[i]) {
                flag1=true;
            }
        }
        if (flag1==false) {
            letra='F';
        }
        return letra;
    }
    protected String comprobarColor(String color){
        boolean flag1=false;
        for (int i = 0; i < colores_disponibles.length; i++) {
            if(color.equals(colores_disponibles[i])){
                flag1=true;
            }
        }
        if (flag1==false) {
            color="blanco";
        }
        return color;
    }
    
    public void precioFinal(){
        switch(consumo_energetico){
            case 'A':
                precio_base+=100;
                break;
            case 'B':
                precio_base+=80;
                break;
            case 'C':
                precio_base+=60;
                break;
            case 'D':
                precio_base+=50;
                break;
            case 'E':
                precio_base+=30;
                break;
            case 'F':
                precio_base+=10;
                break;
            default:
                System.out.println("Consumo incorrecto");
                break;
        }
        if(peso>0 && peso<=19){
            precio_base+=10;
        }
        else if(peso>=20 && peso<=49){
            precio_base+=50;
        }
        else if(peso>=50 && peso<=79){
            precio_base+=80;
        }
        else if(peso>80){
            precio_base+=100;
        }
    }
    public String getTipo() {
        return tipo;
    }
}
